//Requirments
const Discord = require("discord.js");
const request = require("request");
const fs = require("fs");
const portscanner = require('portscanner');

//Initilization
const client = new Discord.Client();
const token = require('./token.json');
require('events').EventEmitter.defaultMaxListeners = Infinity;

//8ball answer file
const answer = require('./8ball.json');

//Monster ingredient file
const ingredient = require('./monster.json');

//Puns file
const pun = require('./puns.json');

client.on("ready", () => {
  console.log(`Logged in as ${client.user.username}!`);
});

client.on("message", msg => {

  //Command prefix
  let prefix = "$";

  //Ignore bot messages
  if (msg.author.bot) return;

  //Help start
  if (msg.content.toLowerCase() === prefix + "help") {
  
    msg.channel.sendMessage("", {embed: {
        color: 12583167,
        description: "Check your DMs c:"
    }});
    
    msg.author.sendMessage("\n```Thank you for choosing me! Here is a basic help page:\n\n1. $avatar (@user) - Sends a users profile pic\n\n2. $command2 - Sends a link to Song 2 by blur\n\n3. $hate - Sends a nice hate song\n\n4. $md5 (string) - Sends the md5 hash of a string\n\n5. $define (word) - Sends the urban dictionary definition of a word\n\n6. $geoip (Geoip) - Sends the geographical information for an IP address\n\n7. $btc - Sends the current exchange rate for 1 BTC\n\n8. $complain (complaint) - Make a complaint about this bot\n\n9. $lyrics (Artist name)~(song name) - Returns a URL to your selected lyrics\n\n10. $8ball (question?) - Ask the 8 ball a question\n\n11. $lmgtfy (query) - Sends a let me google that for you link\n\n12. $mac (00:00:00:00:00:00) - Returns the manufacturer of the device\n\n13. $pscan (host port) - Scan a specified host to check if the port is open\n\n14. $rr - Play a game of Russian Roulette\n\n15. $dox - Dms you the dox template\n\n16. $monster - Returns a random ingredient in monster\n\n17. $forkbomb - Gives you a forkbomb command for Linux Mac or Windows\n\n18. $pun - returns a random pun```");
  }
  //Help end

  //Avatar start
  if (msg.content.toLowerCase().startsWith(prefix + 'avatar <@')) {
    let username =  msg.mentions.users.first()
    msg.reply(username.avatarURL);
  }
  //Avatar end

  //Command2 start
  if (msg.content.toLowerCase() === prefix + "command2") {
    msg.reply("https://www.youtube.com/watch?v=SSbBvKaM6sk");
  }
  //Command2 end

  //Hate start
  if (msg.content.toLowerCase() === prefix + "hate") {
    msg.channel.sendMessage("```I hate niggers,\nI hate jews,\nI hate spics,\nAnd arabs too.\nGo ahead and try to sue,\nI got cash up the wazoo.```");
  }
  //Hate end

  //Noob start
  if (msg.content.toLowerCase().includes("noob")) {
    msg.channel.sendMessage("I fucking hate noobs.");
  }
  //Noob end

  //I fucking hate you start
  if (msg.content.toLowerCase().includes("i fucking hate you")) {
    msg.channel.sendMessage("https://www.youtube.com/watch?v=35rHHEiNaIM");
  }
  //I fucking hate you end

  //Kkk start
  if (msg.content.toLowerCase().includes("kkk")) {
    msg.channel.sendMessage("Fuck niggers");
  }
  //Kkk end

  //@everyone start
  if (msg.mentions.everyone === true) {
    msg.reply("I swear if you don't have a good reason for mentioning everyone I will slaughter your family and waterboard you in their blood c:");
  }
  //@everyone end

  //Md5 start
  if (msg.content.toLowerCase().startsWith(prefix + "md5")) {

    let string = msg.content.slice(5);

    request("https://api.apithis.net/encrypt.php?type=md5&content=" + string, function(error, response, body) {
      msg.channel.sendMessage(body);
    })
  }
  //Md5 end

  //Define start
  if (msg.content.toLowerCase().startsWith(prefix + "define")) {

    let string1 = msg.content.slice(8);
    let string = string1.split(' ').join('');

    request("https://api.apithis.net/dictionary.php?define=" + string, function(error, response, body) {
      msg.channel.sendMessage("**```" + "Definition: " + (body) + "```**");
    })

    request("https://api.apithis.net/dictionary.php?example=" + string, function(error, response, body) {
      msg.channel.sendMessage("**```" + "Example: " + (body) + "```**");
    })

  }
  //Define end

  //Geoip start
  if (msg.content.toLowerCase().startsWith(prefix + "geoip")) {

    let string = msg.content.slice(7);

    request("https://api.apithis.net/geoip.php?ip=" + string, function(error, response, body) {

      let output = (body).split('<br />').join('\n\n')

      msg.channel.sendMessage('**```' + output + '```**');
    })
  }
  //Geoip end

  //Btc start
  if (msg.content.toLowerCase().startsWith(prefix + "btc")) {

    request("https://blockchain.info/ticker", function(error, response, body) {

      var now = new Date();

      var now = now.toString();
      
      var now = now.slice(0,-15);

      let currencies = (body).split("\n").slice(1);
      
      var usd = currencies[1].split(" ").slice(1);

      var usd = usd[8].slice(0,-1);

      var cad = currencies[9].split(" ").slice(1);
      
      var cad = cad[8].slice(0,-1);

      var eur = currencies[6].split(" ").slice(1);
      
      var eur = eur[8].slice(0,-1);

      var gbp = currencies[17].split(" ").slice(1);
      
      var gbp = gbp[8].slice(0,-1);

      msg.channel.sendMessage('**```' + 'Current price of bitcoin as of: ' + now + '\n\n' + 'US Dollar: ' + usd + '\n\n' + 'Canadian Dollar: ' + cad + '\n\n' + 'Euro: ' + eur + '\n\n' + 'British Pound: ' + gbp + '```**');

    })
  }
  //Btc end

  //Ping start
  if (msg.content.toLowerCase().startsWith(prefix + 'ping')) {
      msg.channel.sendMessage("Pong!")
          .then(message => {
              message.edit(`Pong! \`${message.createdTimestamp - msg.createdTimestamp}ms\``);
          });
  };
  //Ping end

  //Complain start
  if (msg.content.toLowerCase().startsWith(prefix + "complain")) {

    let complaint = msg.content.slice(10);

    msg.channel.sendMessage("echo " + "'" + complaint + "'" + " > /dev/null")

  }
  //Complain end

  //Lyrics start
  if (msg.content.toLowerCase().startsWith(prefix + "lyrics")) {

    var urlend = msg.content.slice(8);

    var urlend = (urlend).split("~").join("-").split(" ").join("-");

    msg.channel.sendMessage("https://genius.com/" + urlend + "-lyrics")

  }
  //lyrics end
  
  //8ball start
  if (msg.content.toLowerCase().startsWith(prefix + "8ball")) {
      
    if (msg.content.slice(7).includes("?")) {
    
      var num = Math.floor(Math.random() * ((10-1)+1) + 1);

      msg.channel.sendMessage(answer.ans[num]);
      
    } else {
    
      msg.channel.sendMessage("I'm not answering that unless you put a question mark, learn basic grammar. :middle_finger:");
      
    }

  }
  //8ball end
  
  //Lmgtfy start
  if (msg.content.toLowerCase().startsWith(prefix + "lmgtfy")) {
  
    let query = msg.content.toLowerCase().slice(8).split(" ").join("+");
    
    msg.channel.sendMessage("http://lmgtfy.com/?q=" + query);
    
  }
  //Lmgtfy end
  
  //Mac start
  if (msg.content.toLowerCase().startsWith(prefix + "mac")) {

    let mac = msg.content.slice(5);

    request("https://macvendors.co/api/" + mac + "/pipe", function(error, response, body) {
    
      var out = body.split("|");
      
      var out = out[0].slice(1,-1);
      
      msg.channel.sendMessage("", {embed: {
        color: 12583167,
        description: "Manufacturer: " + out
    }});
    
    })
  }
  //Mac end
  
  //Pscan start
  if (msg.content.toLowerCase().startsWith(prefix + "pscan")) {
  
    var args = msg.content.split(" ").slice(1);
    
    var ip = args[0];
    
    var port = args[1];
        
    portscanner.checkPortStatus(port, ip, function(error, status) {
 
    msg.channel.sendMessage("```Port " + port + " is: " + status + "```")
   
    })
  }
  //Pscan end
  
  //rr start
  if (msg.content.toLowerCase() === prefix + "rr") {
  
    var result = Math.floor(Math.random() * ((5-1)+1) + 1);
    
      if (result == 1 || result == 2) {
      
        msg.channel.sendMessage("Yay, " + msg.author + " died c:");
        
      } else {
      
        msg.channel.sendMessage("Unfortunatly, " + msg.author + " lived :c");
        
      }
      
  }
  //rr end
  
  //dox start
  if (msg.content.toLowerCase() === prefix + "dox") {
  
    msg.channel.sendMessage("", {embed: {
        color: 12583167,
        description: "Check your DMs c:"
    }});
    
    msg.author.sendMessage("\n***- Personal info -***\n\nFull Name:\n\nAge:\n\nBirth Date:\n\nE-mail:\n\nFormer E-mails:\n\nPhone Number(s):\n\nFormer Numbers:\n\nCriminal Record:\n\n***- Geographic Location -***\n\nSchool/University:\n\nCountry:\n\nState:\n\nCity:\n\nCountry Code:\n\nZip Code:\n\nAddress(s):\n\n***- Technology Info -***\n\nIP:\n\nRouter IP:\n\nDNS Servers:\n\nOperating System:\n\nWeb Browser:\n\nPassword(s) used:\n\nSocial Accounts:\n\nSocial Security Number:\n\nCredit Card Info:\n\nBank:\n\nAccount  Balance:\n\nLatest Transaction Amount:\n\n***- Parents Personal Info -***\n\nFull Name:\n\nAge:\n\nBirth Date:\n\nE-mail:\n\nFormer E-mails:\n\nPhone Number(s):\n\nFormer Numbers:\n\nCriminal Record:\n\nTwitter:");
  }
  //dox end
  
  //Monster start
  if (msg.content.toLowerCase() === prefix + "monster") {

    var num = Math.floor(Math.random() * ((21-1)+1) + 1);

    msg.channel.sendMessage(ingredient.ingredient[num]);

  }
  //Monster end
  
  //Forkbomb start
  if (msg.content.toLowerCase() === prefix + "forkbomb") {
  
    msg.channel.sendMessage("```Forkbombs:\n\nLinux/Mac - :(){ :|: & };:\n\nWindows - %0|%0```");
    
  }
  //Forkbomb end
  
  //Pun start
  if (msg.content.toLowerCase() === prefix + "pun") {

    var num = Math.floor(Math.random() * ((30-1)+1) + 1);

    msg.channel.sendMessage(pun.pun[num]);

  }
  //Pun end
  
  //Math.floor(Math.random() * ((y-x)+1) + x);
  //x = start y = end
  //random math shit XD
  
});

client.login(token.token[0]);